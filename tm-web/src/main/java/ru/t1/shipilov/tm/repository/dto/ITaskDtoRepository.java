package ru.t1.shipilov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.dto.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDTO, String> {

    void deleteAllByUserId(@NotNull String userId);

    void deleteByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findByIdAndUserId(@NotNull String id, @NotNull String userId);

}
