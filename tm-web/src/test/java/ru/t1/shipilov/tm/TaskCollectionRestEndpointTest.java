package ru.t1.shipilov.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.client.TaskCollectionRestEndpointClient;
import ru.t1.shipilov.tm.marker.IntegrationCategory;
import ru.t1.shipilov.tm.dto.TaskDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskCollectionRestEndpointTest {

    @NotNull
    final TaskCollectionRestEndpointClient client = TaskCollectionRestEndpointClient.client();

    @NotNull
    final TaskDTO taskAutoFirst = new TaskDTO("Task Test 1");

    @NotNull
    final TaskDTO taskAutoSecond = new TaskDTO("Task Test 2");

    @NotNull
    final TaskDTO taskAutoThird = new TaskDTO("Task Test 3");

    @NotNull
    final List<TaskDTO> taskCollection = Arrays.asList(taskAutoFirst, taskAutoSecond, taskAutoThird);

    @NotNull
    final TaskDTO taskManualFirst = new TaskDTO("Task Manual Test 1");

    @NotNull
    final TaskDTO taskManualSecond = new TaskDTO("Task Manual Test 2");

    @NotNull
    final List<TaskDTO> taskCollectionSecond = Arrays.asList(taskManualFirst, taskManualSecond);

    @Before
    public void init() {
        client.post(taskCollection);
    }

    @After
    public void clear() {
        client.delete(client.get());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get());
        for (final TaskDTO task : taskCollection) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPost() {
        Assert.assertEquals(3, client.get().size());
        client.post(taskCollectionSecond);
        Assert.assertNotNull(client.get());
        Assert.assertEquals(5, client.get().size());
        for (final TaskDTO task : taskCollectionSecond) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPut() {
        for (final TaskDTO task : client.get()) {
            Assert.assertNull(task.getDescription());
        }
        taskCollection.get(0).setDescription("Description 1");
        taskCollection.get(1).setDescription("Description 2");
        taskCollection.get(2).setDescription("Description 3");
        client.put(taskCollection);
        for (final TaskDTO task : client.get()) {
            Assert.assertNotNull(task.getDescription());
        }
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get());
        client.delete(taskCollection);
        Assert.assertEquals(Collections.emptyList(), client.get());
    }

}
