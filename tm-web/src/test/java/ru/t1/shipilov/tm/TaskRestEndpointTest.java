package ru.t1.shipilov.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.client.TaskRestEndpointClient;
import ru.t1.shipilov.tm.marker.IntegrationCategory;
import ru.t1.shipilov.tm.dto.TaskDTO;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final TaskDTO taskManual = new TaskDTO("Task MANUAL TEST");

    @NotNull
    final TaskDTO taskAutoFirst = new TaskDTO("Task Test 1");


    @Before
    public void init() {
        client.post(taskAutoFirst);
    }

    @After
    public void clear() {
        client.delete(taskAutoFirst.getId());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get(taskAutoFirst.getId()));
        Assert.assertThrows(FeignException.class, () -> client.get(taskManual.getId()));
    }

    @Test
    public void testPost() {
        Assert.assertThrows(FeignException.class, () -> client.get(taskManual.getId()));
        client.post(taskManual);
        Assert.assertNotNull(client.get(taskManual.getId()));
        client.delete(taskManual.getId());
    }

    @Test
    public void testPut() {
        Assert.assertNull(client.get(taskAutoFirst.getId()).getDescription());
        taskAutoFirst.setDescription("Description 1");
        client.put(taskAutoFirst);
        Assert.assertEquals("Description 1", client.get(taskAutoFirst.getId()).getDescription());
        taskAutoFirst.setDescription(null);
        client.put(taskAutoFirst);
        Assert.assertNull(client.get(taskAutoFirst.getId()).getDescription());
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get(taskAutoFirst.getId()));
        client.delete(taskAutoFirst.getId());
        Assert.assertThrows(FeignException.class, () -> client.get(taskAutoFirst.getId()));
        client.post(taskAutoFirst);
    }

}
