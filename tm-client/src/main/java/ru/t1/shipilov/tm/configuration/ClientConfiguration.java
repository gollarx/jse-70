package ru.t1.shipilov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.shipilov.tm.api.endpoint.*;
import ru.t1.shipilov.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.shipilov.tm")
public class ClientConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        @NotNull final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
        return projectEndpoint;
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        @NotNull final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);
        return systemEndpoint;
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        @NotNull final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);
        return taskEndpoint;
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        @NotNull final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);
        return userEndpoint;
    }

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        return authEndpoint;
    }

}
