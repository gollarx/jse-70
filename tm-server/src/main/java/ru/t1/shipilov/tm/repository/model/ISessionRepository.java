package ru.t1.shipilov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.model.Session;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {
}

