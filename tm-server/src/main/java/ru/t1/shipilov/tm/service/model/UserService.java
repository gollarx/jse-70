package ru.t1.shipilov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.repository.model.IProjectRepository;
import ru.t1.shipilov.tm.repository.model.ITaskRepository;
import ru.t1.shipilov.tm.repository.model.IUserRepository;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.api.service.model.IUserService;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.exception.entity.EntityNotFoundException;
import ru.t1.shipilov.tm.exception.entity.UserNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.EmailEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.IdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.LoginEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.PasswordEmptyException;
import ru.t1.shipilov.tm.exception.user.ExistsEmailException;
import ru.t1.shipilov.tm.exception.user.ExistsLoginException;
import ru.t1.shipilov.tm.exception.user.RoleEmptyException;
import ru.t1.shipilov.tm.model.User;
import ru.t1.shipilov.tm.util.HashUtil;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;


    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull User user = new User();
        if (repository.existsByLogin(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        if (email != null && !email.isEmpty()) {
            if (repository.existsByEmail(email)) throw new ExistsEmailException();
            user.setEmail(email);
        }
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull User user = new User();
        if (repository.existsByLogin(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.save(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable User user) {
        if (user == null) throw new EntityNotFoundException();
        repository.save(user);
    }

    @Override
    @SneakyThrows
    public @NotNull List<User> findAll() {
        @NotNull List<User> result;
        result = repository.findAll();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user;
        user = repository.findById(id).orElse(null);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        user = repository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user;
        user = repository.findByEmail(email);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user;
        user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        taskRepository.deleteAllByUserId(user.getId());
        projectRepository.deleteAllByUserId(user.getId());
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.deleteAllByUserId(userId);
        projectRepository.deleteAllByUserId(userId);
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User user;
        user = repository.findById(id).orElse(null);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user;
        user = repository.findById(id).orElse(null);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        boolean result;
        result = repository.existsByLogin(login);
        return result;
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        boolean result;
        result = repository.existsByEmail(email);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.save(user);
    }

}
