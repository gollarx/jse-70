package ru.t1.shipilov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskDtoService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void removeProjectByIndex(@Nullable String userId, @Nullable Integer projectIndex);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}

